import OutputSpeechType from '@agrozyme/alexa-skill-type/source/Custom/Enumeration/OutputSpeechType';
import Skill from '../Skill';
import Base from './Base';

export type ControllerConstructor = { new(skill: Skill): Controller };

abstract class Controller extends Base {
  readonly skill: Skill;

  // noinspection TypeScriptAbstractClassConstructorCanBeMadeProtected
  constructor(skill: Skill) {
    super();
    this.skill = skill;
  }

  protected outputSpeechText(text: string, shouldEndSession: boolean = true) {
    const response = this.skill.response.response;
    response.outputSpeech = {
      type: OutputSpeechType.PlainText,
      text
    };
    response.shouldEndSession = shouldEndSession;
  }

  protected outputSpeechMarkup(ssml: string, shouldEndSession: boolean = true) {
    const response = this.skill.response.response;
    response.outputSpeech = {
      type: OutputSpeechType.SSML,
      text: ssml
    };
    response.shouldEndSession = shouldEndSession;
  }

  async run(): Promise<any> {
  }

}

export default Controller;
