import Base from '../Controller/Intent';

class Error extends Base {
  async run(): Promise<any> {
    this.outputSpeechText('no match intent');
  }

}

export default Error;
